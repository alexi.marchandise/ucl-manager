﻿using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.EventArgs;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using UCLManager.Commands;
using UCLManager.EventHandlers;
using UCLManager.Enums;
using UCLManager.Models;
using UCLManager.Services;
using System;
// using UCLManager.DataFormatter;

namespace UCLManager.Bots
{
    public class Bot
    {
        public DiscordClient Client { get; private set; }
        public  CommandsNextExtension Commands { get; private set; }
        public static ConfigJson ConfigJson { get; private set; }

        public async Task RunAsync()
        {
            #region Read Json configuration file
            ConfigJson = ConfigReader.ReadFileAsync("config.json").Result;
            #endregion

            #region Setup DiscordClient
            DiscordConfiguration config = new()
            {
                Token = ConfigJson.Token,
                TokenType = TokenType.Bot,
                AutoReconnect = true,
                MinimumLogLevel = LogLevel.Debug,
            };

            Client = new DiscordClient(config);
            #endregion

            #region Setup Discord Interactivity
            Client.UseInteractivity(new InteractivityConfiguration
            {
                PollBehaviour = DSharpPlus.Interactivity.Enums.PollBehaviour.KeepEmojis,
                Timeout = TimeSpan.FromMinutes(5),
            });
            #endregion

            #region Register all event listeners
            RegisterEventListeners();
            #endregion

            #region Setup Commands Configuration
            CommandsNextConfiguration commandsConfig = new()
            {
                StringPrefixes = new string[] { ConfigJson.Prefix },
                EnableDms = true,
                EnableMentionPrefix = true,
                DmHelp = true,
                IgnoreExtraArguments = true,
            };

            Commands = Client.UseCommandsNext(commandsConfig);
            #endregion

            #region Register commands
            Commands.RegisterCommands<UCLCommands>();
            Commands.RegisterCommands<FeedbackCommands>();
            #endregion

            await Client.ConnectAsync();

            await Task.Delay(-1);
        }

        private async Task OnClientConnected(DiscordClient client, SocketEventArgs e)
        {
            DiscordChannel channel = await client.GetChannelAsync(ConfigJson.LogRegistryChannelId);
            DiscordEmbedBuilder embed = EmbedTemplateBuilder.GetEmbedTemplate()
                .WithDescription("Je suis de retour online !")
            ;

            await channel.SendMessageAsync(embed);
        }

        private async Task OnCLientExit(ConsoleCtrlHandlerCode eventCode)
        {
            DiscordChannel channel = await Client.GetChannelAsync(ConfigJson.LogRegistryChannelId);
            DiscordEmbedBuilder embed = EmbedTemplateBuilder.GetEmbedTemplate()
                .WithDescription("Je me déconnecte, à plus tard !")
            ;

            await channel.SendMessageAsync(embed);
        }

        private void RegisterEventListeners()
        {
            /** Send a message when client has started up */
            Client.SocketOpened += OnClientConnected;
            /** Send a message when client is being exited */
            new ExitHandler(OnCLientExit);
        }
    }
}