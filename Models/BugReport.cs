﻿using DSharpPlus.Entities;
using System;
using System.Threading.Tasks;
using UCLManager.Bots;
using UCLManager.Services;

namespace UCLManager.Models
{
    class BugReport : AbstractFeedback
    {
        public override async Task NotificateAsync()
        {
            DiscordUser BotAuthor = Bot.ConfigJson.GetBotAuthor(Ctx.Client).Result;
            DiscordEmbedBuilder embed = EmbedTemplateBuilder.GetEmbedTemplate()
                .WithAuthor(BotAuthor.Username, "", BotAuthor.GetAvatarUrl(DSharpPlus.ImageFormat.Auto))
                .WithTitle("Succès !")
                .WithDescription(
                    "Ton signalement m'est bien parvenu\n" +
                    $"Merci pour ton soutien { Ctx.Message.Author.Mention } !"
                )
            ;
            await Ctx.Channel.SendMessageAsync(embed).ConfigureAwait(false);
        }

        public override async Task SendAsync()
        {
            await Ctx.Message.DeleteAsync().ConfigureAwait(false);

            DiscordUser user = Ctx.User;
            DiscordChannel ideaChannel = await Ctx.Client.GetChannelAsync(ChannelId).ConfigureAwait(false);

            DiscordEmbedBuilder embed = EmbedTemplateBuilder.GetEmbedTemplate()
                .WithAuthor($"{ user.Username }#{ user.Discriminator }", "", user.AvatarUrl)
                .WithTitle($"Rapport de bug :")
                .WithDescription($"Rapport de { user.Mention } :\n { Message }")
                .WithTimestamp(DateTime.Now)
                .WithColor(DiscordColor.Red)
            ;
            await ideaChannel.SendMessageAsync(embed).ConfigureAwait(false);

        }
    }
}
