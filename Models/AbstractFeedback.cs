﻿using DSharpPlus.CommandsNext;
using System.Threading.Tasks;

namespace UCLManager.Models
{
    abstract class AbstractFeedback
    {
        protected CommandContext Ctx { get; set; }
        protected ulong ChannelId { get; set; }
        protected string Message { get; set; }

        public AbstractFeedback WithCtx(CommandContext ctx)
        {
            Ctx = ctx;

            return this;
        }

        public AbstractFeedback WithChannelId(ulong channelId)
        {
            ChannelId = channelId;

            return this;
        }

        public AbstractFeedback WithMessage(string message)
        {
            Message = message;

            return this;
        }

        public abstract Task NotificateAsync();
        public abstract Task SendAsync();
    }
}
