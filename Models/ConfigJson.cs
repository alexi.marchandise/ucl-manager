﻿using DSharpPlus;
using DSharpPlus.Entities;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace UCLManager.Models
{
    public class ConfigJson
    {
        [JsonProperty("token")]
        public string Token { get; private set; }

        [JsonProperty("prefix")]
        public string Prefix { get; private set; }

        [JsonProperty("bot_author_id")]
        public ulong BotAuthorId { get; private set; }

        [JsonProperty("log_registry_channel_id")]
        public ulong LogRegistryChannelId { get; private set; }

        private DiscordChannel LogRegistry { get; set; } = null;
        private DiscordUser BotAuthor { get; set; } = null;

        public async Task<DiscordChannel> GetLogRegistry(DiscordClient client)
        {
            if (LogRegistry == null)
            {
                LogRegistry = await client.GetChannelAsync(LogRegistryChannelId).ConfigureAwait(false);
            }

            return LogRegistry;
        }

        public async Task<DiscordUser> GetBotAuthor(DiscordClient client)
        {
            if (LogRegistry == null)
            {
                BotAuthor = await client.GetUserAsync(BotAuthorId).ConfigureAwait(false);
            }

            return BotAuthor;
        }
    }
}