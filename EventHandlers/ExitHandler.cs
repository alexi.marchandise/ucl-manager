﻿using System.Runtime.InteropServices;
using System.Threading.Tasks;
using UCLManager.Enums;

namespace UCLManager.EventHandlers
{
    class ExitHandler
    {
        #region Page Event Setup
        public delegate Task ConsoleCtrlHandlerDelegate(ConsoleCtrlHandlerCode eventCode);
        [DllImport("kernel32.dll")]
        public static extern void SetConsoleCtrlHandler(ConsoleCtrlHandlerDelegate handlerProc, bool add);
        static ConsoleCtrlHandlerDelegate _consoleHandler;
        #endregion
        public ExitHandler(ConsoleCtrlHandlerDelegate consoleHandler)
        {
            _consoleHandler = new ConsoleCtrlHandlerDelegate(consoleHandler);
            SetConsoleCtrlHandler(_consoleHandler, true);
        }
    }
}
