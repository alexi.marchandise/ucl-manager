﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UCLManager.Enums
{
    public enum FeedbackTypeEnum
    {
        Idea = 0,
        BugReport = 1,
    }
}
