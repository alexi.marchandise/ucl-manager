﻿using DSharpPlus.CommandsNext;
using System;
using UCLManager.Enums;
using UCLManager.Models;

namespace UCLManager.Services
{
    class FeedbackBuilder
    {
        public AbstractFeedback Feedback { get; private set; }

        public FeedbackBuilder(CommandContext ctx, FeedbackTypeEnum type, ulong channelId, string message)
        {
            Feedback = type switch
            {
                FeedbackTypeEnum.Idea => new Idea(),
                FeedbackTypeEnum.BugReport => new BugReport(),
                _ => throw new Exception("This FeedbackType does not exist, please refer to the FeedbackTypeEnum."),
            };

            Feedback
                .WithCtx(ctx)
                .WithChannelId(channelId)
                .WithMessage(message)
            ;
        }
    }
}
