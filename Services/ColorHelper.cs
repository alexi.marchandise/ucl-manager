﻿using DSharpPlus.Entities;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace UCLManager.Services
{
    public class ColorHelper
    {
        public const string StringifiedColors = "Goldenrod, Azure, Rose, " +
            "SpringGreen, Chartreuse, Orange, " + "Purple, Violet, Brown, " +
            "HotPink, CornFlowerBlue, Gold, " + "MidnightBlue, Wheat, IndianRed, " +
            "Turquoise, SapGreen, PhthaloBlue, " + "Sienna, Lilac, Aquamarine, " +
            "Teal, Magenta, None, " + "Black, White, Gray, " + "DarkGray, LightGray, VeryDarkGray, " +
            "Grayple, DarkButNotBlack, Blurple, " + "Red, DarkRed, Green, " +
            "DarkGreen, Blue, DarkBlue, " + "Yellow, Cyan, NotQuiteBlack";
        public static readonly DiscordColor Goldenrod = DiscordColor.Goldenrod;
        public static readonly DiscordColor Azure = DiscordColor.Azure;
        public static readonly DiscordColor Rose = DiscordColor.Rose;
        public static readonly DiscordColor SpringGreen = DiscordColor.SpringGreen;
        public static readonly DiscordColor Chartreuse = DiscordColor.Chartreuse;
        public static readonly DiscordColor Orange = DiscordColor.Orange;
        public static readonly DiscordColor Purple = DiscordColor.Purple;
        public static readonly DiscordColor Violet = DiscordColor.Violet;
        public static readonly DiscordColor Brown = DiscordColor.Brown;
        public static readonly DiscordColor HotPink = DiscordColor.HotPink;
        public static readonly DiscordColor CornflowerBlue = DiscordColor.CornflowerBlue;
        public static readonly DiscordColor Gold = DiscordColor.Gold;
        public static readonly DiscordColor MidnightBlue = DiscordColor.MidnightBlue;
        public static readonly DiscordColor Wheat = DiscordColor.Wheat;
        public static readonly DiscordColor IndianRed = DiscordColor.IndianRed;
        public static readonly DiscordColor Turquoise = DiscordColor.Turquoise;
        public static readonly DiscordColor SapGreen = DiscordColor.SapGreen;
        public static readonly DiscordColor PhthaloBlue = DiscordColor.PhthaloBlue;
        public static readonly DiscordColor PhthaloGreen = DiscordColor.PhthaloGreen;
        public static readonly DiscordColor Sienna = DiscordColor.Sienna;
        public static readonly DiscordColor Lilac = DiscordColor.Lilac;
        public static readonly DiscordColor Aquamarine = DiscordColor.Aquamarine;
        public static readonly DiscordColor Teal = DiscordColor.Teal;
        public static readonly DiscordColor Magenta = DiscordColor.Magenta;
        public static readonly DiscordColor None = DiscordColor.None;
        public static readonly DiscordColor Black = DiscordColor.Black;
        public static readonly DiscordColor White = DiscordColor.White;
        public static readonly DiscordColor Gray = DiscordColor.Gray;
        public static readonly DiscordColor DarkGray = DiscordColor.DarkGray;
        public static readonly DiscordColor LightGray = DiscordColor.LightGray;
        public static readonly DiscordColor VeryDarkGray = DiscordColor.VeryDarkGray;
        public static readonly DiscordColor Grayple = DiscordColor.Grayple;
        public static readonly DiscordColor DarkButNotBlack = DiscordColor.DarkButNotBlack;
        public static readonly DiscordColor Blurple = DiscordColor.Blurple;
        public static readonly DiscordColor Red = DiscordColor.Red;
        public static readonly DiscordColor DarkRed = DiscordColor.DarkRed;
        public static readonly DiscordColor Green = DiscordColor.Green;
        public static readonly DiscordColor DarkGreen = DiscordColor.DarkGreen;
        public static readonly DiscordColor Blue = DiscordColor.Blue;
        public static readonly DiscordColor DarkBlue = DiscordColor.DarkBlue;
        public static readonly DiscordColor Yellow = DiscordColor.Yellow;
        public static readonly DiscordColor Cyan = DiscordColor.Cyan;
        public static readonly DiscordColor NotQuiteBlack = DiscordColor.NotQuiteBlack;

        public static List<string> ToList()
        {
            PropertyInfo[] discordColorProperties = new DiscordColor().GetType().GetProperties();
            ArrayList undesiredAttributes = new ArrayList(new string[] { "R", "G", "B" });
            List<string> colors = new List<string>();

            foreach (PropertyInfo property in discordColorProperties)
            {
                if (!undesiredAttributes.Contains(property.Name))
                {
                    colors.Add(property.Name);
                }
            }

            return colors;
        }

        public DiscordColor GuessDiscordColor(string color)
        {
            FieldInfo colorHelperMethod = this.GetType().GetField(color);
            return (DiscordColor)colorHelperMethod.GetValue(this);
        }
    }
}
