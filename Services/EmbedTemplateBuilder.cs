﻿using DSharpPlus.Entities;

namespace UCLManager.Services
{
    public class EmbedTemplateBuilder
    {
        public static DiscordEmbedBuilder GetEmbedTemplate()
        {
            return new DiscordEmbedBuilder()
                .WithColor(DiscordColor.Yellow)
            ;
        }
    }
}
