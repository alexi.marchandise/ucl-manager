﻿using UCLManager.Models;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace UCLManager.Services
{
    class ConfigReader
    {
        public static async Task<ConfigJson> ReadFileAsync(string fileName)
        {
            var json = string.Empty;

            using (var fs = File.OpenRead("config/" + fileName))
            using (var sr = new StreamReader(fs, new UTF8Encoding(false)))
                json = await sr.ReadToEndAsync().ConfigureAwait(false);

            return JsonConvert.DeserializeObject<ConfigJson>(json);
        }
    }

    
}
