﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using System;
using System.Threading.Tasks;
using UCLManager.Services;

namespace UCLManager.Commands
{
    public class UCLCommands : BaseCommandModule
    {
        readonly string[] PingResponses = new string[]
        {
            "Pong !",
            "Ping ? Voulais-tu dire \"Pingouin\" ?",
            "Shut up idiot :disappointed:",
            "Another one ?",
            "/ping\n" +
            "Aller réponds esclave\n" +
            "C'est quand même pas au bot de tout faire ici !\n",
            "Longue vie à UCL ! :UCL:",
        };

        [Command("Ping"), Description("Renvoie une réponse aléatoire")]
        public async Task Ping(CommandContext ctx)
        {
            await ctx.TriggerTypingAsync();

            int responseIndex = new Random().Next(PingResponses.Length);
            string response = PingResponses[responseIndex];

            await ctx.Channel.SendMessageAsync(response).ConfigureAwait(false);
        }

        [Command("Embed"), Description("Envoie un message intégré")]
        public async Task Embed(CommandContext ctx,
            [Description("\nLe contenu du message que tu souhaites envoyer, à écrire entre guillemets")]
            string content,
            [Description("\n(Facultatif) Permet de choisir une couleur de bordure pour ton message.\n " +
                        "Les couleurs dispoibles sont : " + ColorHelper.StringifiedColors)]
            string color = ""
        )
        {
            await ctx.TriggerTypingAsync();

            #region Set Embed Template
            DiscordEmbedBuilder embed = EmbedTemplateBuilder.GetEmbedTemplate();
            #endregion

            if (string.Empty != color)
            {
                #region Preset DiscordColor
                DiscordColor discordColor;
                #endregion

                try
                {
                    #region Guess Embed color based on input color
                    discordColor = new ColorHelper().GuessDiscordColor(color);
                    #endregion
                }
                catch (Exception e)
                {
                    #region Shoot error to the user and stop command
                    embed
                        .WithTitle("BOT_ERROR")
                        .WithDescription($"La couleur \"{ color }\" n'existe pas")
                    ;
                    await ctx.Channel.SendMessageAsync(embed);
                    return;
                    #endregion
                }

                #region Update Embed's DiscordColor 
                embed.WithColor(discordColor);
                #endregion
            }

            #region Set Embed's title and description
            embed
                .WithAuthor(ctx.User.Username, "", ctx.User.AvatarUrl)
                .WithDescription(content)
            ;
            #endregion

            #region Send Embed message
            await ctx.Channel.SendMessageAsync(embed);
            #endregion
        }

        /** TODO: continue testings */
        /**[Command("Embed"), Description("Envoie un message intégré")]
        public async Task Embed(CommandContext ctx,
                [Description("\nLe contenu du message que tu souhaites envoyer, à écrire entre guillemets")]
                string content,
                [Description("\nLe channel où envoyer le message")]
                ulong channelId,
                [Description("\n(Facultatif) Permet de choisir une couleur de bordure pour ton message.\n " +
                        "Les couleurs dispoibles sont : " + ColorHelper.StringifiedColors)]
                string color = ""

            )
        {
            await Task.CompletedTask;
        }**/


        [Command("Pingouin"), Hidden, Description("Renvoie un pingouin, qui n'en voudrait pas ?")]
        public async Task Penguin(CommandContext ctx)
        {
            await ctx.TriggerTypingAsync();
            await ctx.Channel.SendMessageAsync(":penguin:").ConfigureAwait(false);
        }
    }
}
