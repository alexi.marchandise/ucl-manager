﻿using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using UCLManager.Services;
using System.Threading.Tasks;
using UCLManager.Models;

namespace UCLManager.Commands
{
    public class FeedbackCommands : BaseCommandModule
    {
        [Command("Idée"), Description("Permet de m'envoyer vos idées"), RequireBotPermissions(Permissions.ManageMessages)]
        public async Task Idea(CommandContext ctx, [Description("\nL'idée que tu souhaites m'envoyer"), RemainingText] string idea)
        {
            await ctx.TriggerTypingAsync();

            Idea feedback = (Idea)new FeedbackBuilder(ctx, type: Enums.FeedbackTypeEnum.Idea, channelId: 716923269839585331, message: idea).Feedback;
            await feedback.SendAsync();
            await feedback.NotificateAsync();
        }

        [Command("Bug"), Description("Permet de me rapporter des bugs"), RequireBotPermissions(Permissions.ManageMessages)]
        public async Task Bug(CommandContext ctx, [Description("\nLe rapport de bug que tu souhaites m'envoyer"), RemainingText] string bug)
        {
            await ctx.TriggerTypingAsync();

            BugReport feedback = (BugReport)new FeedbackBuilder(ctx, type: Enums.FeedbackTypeEnum.BugReport, channelId: 823895318584098836, message: bug).Feedback;
            await feedback.SendAsync();
            await feedback.NotificateAsync();
        }
    }
}
